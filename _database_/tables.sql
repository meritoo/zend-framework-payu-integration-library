--
-- Struktura tabeli dla tabeli `payu_payment_types`
--

CREATE TABLE IF NOT EXISTS `payu_payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `img` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=29 ;

--
-- Zrzut danych tabeli `payu_payment_types`
--

INSERT INTO `payu_payment_types` (`id`, `name`, `type`, `min`, `max`, `img`, `enable`) VALUES
(1, 'mTransfer', 'm', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-m.gif', 1),
(2, 'MultiTransfer', 'n', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-n.gif', 1),
(3, 'Przelew24 BZWBK', 'w', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-w.gif', 1),
(4, 'Pekao24Przelew', 'o', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-o.gif', 1),
(5, 'Płacę poprzez Przelew z BPH', 'h', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-h.gif', 1),
(6, 'Płacę z Inteligo', 'i', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-i.gif', 1),
(7, 'Płacę z Nordea', 'd', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-d.gif', 1),
(8, 'Płacę z iPKO', 'p', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-p.gif', 1),
(9, 'Płać z ING', 'g', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-g.gif', 1),
(10, 'Crédit Agricole e-przelew', 'l', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-l.gif', 1),
(11, 'Przelew z Raiffeisen Bankiem', 'wr', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wr.gif', 1),
(12, 'Płacę z Millennium', 'wm', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wm.gif', 1),
(13, 'Przelew z BGŻ', 'wg', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wg.gif', 1),
(14, 'Przelew z Kredyt Bankiem', 'wk', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wk.gif', 1),
(15, 'Przelew z Polbankiem', 'wp', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wp.gif', 1),
(16, 'Płacę z Citi Handlowy', 'wc', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wc.gif', 1),
(17, 'Przelew z Deutsche Bank', 'wd', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wd.gif', 1),
(18, 'Przelew z Getin Bankiem', 'wi', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wi.gif', 1),
(19, 'Przelew z Invest Bankiem', 'wn', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/off-wn.gif', 1),
(20, 'Przelew z Bankiem Pocztowym', 'wy', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-wy.gif', 1),
(21, 'Płacę przelewem tradycyjnym', 'b', 0.5, 100000000, 'https://www.platnosci.pl/static/images/paytype/on-b.gif', 1),
(22, 'płatnosc testowa', 't', 0.5, 1000, 'https://www.platnosci.pl/static/images/paytype/on-t.gif', 1),
(23, 'Przelew z Idea Bank', 'ib', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-ib.gif', 1),
(24, 'Płacę z Eurobankiem', 'u', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-u.gif', 1),
(25, 'Płacę z Alior Bankiem', 'ab', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-ab.gif', 1),
(26, 'Płacę z Alior Sync', 'as', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-as.gif', 1),
(27, 'Przelew z PBS', 'ps', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/off-ps.gif', 1),
(28, 'MeritumBank Przelew', 'me', 0.5, 1000000, 'https://www.platnosci.pl/static/images/paytype/on-me.gif', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `payu_transactions`
--

CREATE TABLE IF NOT EXISTS `payu_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `pos_id` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `pay_type` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `session_id` varchar(70) COLLATE utf8_polish_ci NOT NULL,
  `amount_ps` float NOT NULL,
  `order_id` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `error_no` smallint(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1;