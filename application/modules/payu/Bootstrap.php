<?php

/**
 * Bootstrap for the PayU module
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_Bootstrap extends Zend_Application_Module_Bootstrap {

    /**
     * Initializes autoloader
     * @return \Zend_Application_Module_Autoloader
     */
    protected function _initAppAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'PayU',
            'basePath' => dirname(__FILE__),
        ));

        return $autoloader;
    }

    /**
     * Initializes configuration
     * @return \Zend_Config
     */
    protected function _initConfig() {
        $configuration = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('configuration', $configuration);

        $fullConfiguration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        Zend_Registry::set('fullConfiguration', $fullConfiguration);

        return $configuration;
    }

    /**
     * Initializes router
     * @return \Zend_Controller_Router_Rewrite
     */
    protected function _initRoutes() {
        $path = __DIR__ . '/configs/routes.ini';

        if (!is_readable($path)) {
            throw new Zend_Exception(sprintf('The PayU module requires routes definition in "%s" file, but the file is missing or is not readable. Check and repair it.', $path));
        }

        $routes = new Zend_Config_Ini($path, APPLICATION_ENV);

        return Zend_Controller_Front::getInstance()
                        ->getRouter()
                        ->addConfig($routes, 'routes');
    }

}