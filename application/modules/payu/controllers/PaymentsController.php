<?php

/**
  Provides functionality for the PayU payments
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_PaymentsController extends Zend_Controller_Action {

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();
    }

    /**
     * Loads payment types
     * @return void
     * 
     * @throws \Exception
     */
    public function importTypesAction() {
        $configuration = Meritoo_PayU_Configuration::getInstance();
        $parameters = $configuration->getPaymentTypesUrlParameters();

        $secretKey = $this->getRequest()
                ->getParam('secretKey');

        if (trim($secretKey) == $parameters->secret_key) {
            $urlTemplate = $parameters->template;
            $urlDomain = $parameters->domain;
            $urlEncoding = $parameters->encoding;

            $posId = $configuration->getPosId();
            $key1 = $configuration->getKey1();
            //$key2 = $configuration->getKey2();
            //$posAuthenticationKey = $configuration->getPosAuthenticationKey();

            $url = sprintf($urlTemplate, $urlDomain, $urlEncoding, $posId, substr($key1, 0, 2));
            $handle = curl_init();

            curl_setopt_array($handle, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR => false
            ));

            $xmlString = curl_exec($handle);

            $error = curl_error($handle);
            $errorNo = curl_errno($handle);

            if (!empty($error)) {
                throw new Zend_Exception(sprintf('Ooops, an error occured. Look at this: "%s" (error number: %s).', $error, $errorNo));
            }

            //die($xmlString);

            if (!empty($xmlString)) {
                $xml = new \SimpleXMLElement($xmlString);
                $communique = 'There is no payment types. Check configuration of your PayU account.';

                if ($xml->children()->count() > 0) {
                    $table = new PayU_Model_DbTable_PaymentTypes();
                    $table->truncate();

                    foreach ($xml->children() as $type) {
                        $table->insert(array(
                            'name' => $type->name,
                            'type' => $type->type,
                            'min' => $type->min,
                            'max' => $type->max,
                            'img' => $type->img,
                            'enable' => (boolean) $type->enable
                        ));
                    }

                    $communique = 'Succesfuly loaded / imported';
                }

                die($communique);
            }
        }
    }

}