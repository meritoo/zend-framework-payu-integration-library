<?php

/**
 * Provides functionality for the PayU transactions
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_TransactionsController extends Zend_Controller_Action {

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();
    }

    /**
     * The "back url" used to process data received when transaction is saved
     * @return void
     */
    public function backUrlAction() {
        $request = $this->getRequest();

        $transId = $request->getParam('transId');
        $posId = $request->getParam('posId');
        $payType = $request->getParam('payType');
        $sessionId = $request->getParam('sessionId');
        $amountPS = $request->getParam('amountPS');
        $orderId = $request->getParam('orderId');
        $error = $request->getParam('error');

        /*
         * Saving transaction
         */
        $transaction = new PayU_Model_Transaction(array(
            'trans_id' => $transId,
            'pos_id' => $posId,
            'pay_type' => $payType,
            'session_id' => $sessionId,
            'amount_ps' => $amountPS,
            'order_id' => $orderId,
            'error_no' => $error
        ));

        $transactionMapper = new PayU_Model_Mapper_Transaction();
        $transactionId = $transactionMapper->addTransaction($transaction);
        $transaction->setId($transactionId);

        //var_dump($transId, $posId, $payType, $sessionId, $amountPS, $orderId, $error, $request->getParams());die;

        /*
         * Saving the transaction in session
         */
        Meritoo_PayU_Transaction_Helper::getInstance()
                ->storeLatestTransaction($transaction);

        /*
         * Redirecting
         */
        $transactionPositive = empty($error);
        $routeName = Meritoo_PayU_Configuration::getInstance()
                ->getTransactionFinishedRouteName($transactionPositive);

        $url = $this->getHelper('url')
                ->url(array(), $routeName);

        $this->_redirect($url);
    }

    /**
     * The "report url" used to process data received when transaction is saved
     * @return void
     */
    /* public function reportUrlAction() {
      $request = $this->getRequest();
      } */
}