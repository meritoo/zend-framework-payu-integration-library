<?php

/**
 * The table with payment types
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_Model_DbTable_PaymentTypes extends Meritoo_Db_Table_Abstract {

    /**
     * {@inheritdoc}
     */
    protected $_name = 'payu_payment_types';

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();
    }

}