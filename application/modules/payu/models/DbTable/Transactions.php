<?php

/**
 * The table with transactions
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_Model_DbTable_Transactions extends Meritoo_Db_Table_Abstract {

    /**
     * {@inheritdoc}
     */
    protected $_name = 'payu_transactions';

}