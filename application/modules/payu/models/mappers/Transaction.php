<?php

/**
 * Mapper for the Transaction model
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class PayU_Model_Mapper_Transaction extends Meritoo_Model_Mapper_Abstract {

    /**
     * {@inheritdoc}
     */
    public function __construct($dbTable = null) {
        if ($dbTable === null) {
            $dbTable = 'PayU_Model_DbTable_Transactions';
        }

        $this->setDbTable($dbTable);
    }

    /**
     * Adds a new transaction
     * 
     * @param PayU_Model_Transaction $transaction The new transaction
     * @return integer
     */
    public function addTransaction(PayU_Model_Transaction $transaction) {
        return $this->getDbTable()
                        ->insert($transaction->toArray());
    }

}