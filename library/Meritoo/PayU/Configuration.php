<?php

/**
 * Configuration of the PayU payment
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_PayU_Configuration {

    /**
     * Class instance (serve a singleton pattern purpose)
     * @var \Meritoo_PayU_Configuration
     */
    private static $instance;

    /**
     * The configuration
     * @var \Zend_Config_Ini
     */
    private $configuration = null;

    /**
     * Returns name of the section used by configuration file
     * @var string
     */
    private $sectionName = 'PayU';

    /**
     * Template for the url used to communicate with PayU
     * @var string
     */
    private $urlTemplate = null;

    /**
     * Encoding of the url used to communicate with PayU
     * @var string
     */
    private $urlEncoding = 'UTF';

    /**
     * ID of payment point, the pos_id parameter
     * @var string
     */
    private $posId = '';

    /**
     * First key
     * @var string
     */
    private $key1 = '';

    /**
     * Second key
     * @var string
     */
    private $key2 = '';

    /**
     * Payment authentication key, the pos_auth_key parameter
     * @var string
     */
    private $posAuthenticationKey = '';

    /**
     * Names of parameters required by transaction
     * @var \Zend_Config_Ini
     */
    private $transactionRequiredParameters = array();

    /**
     * Names of the routes for url where user is redirected when transaction is finished.
     * Contains routes for redirection when transaction was positive and negative.
     * 
     * @var array
     */
    private $transactionFinishedRouteNames = array();

    /**
     * Class constructor
     * @return void
     */
    private function __construct() {
        $this->loadConfiguration();
    }

    /**
     * Returns class instance (serve a singleton pattern purpose)
     * @return \Meritoo_PayU_Configuration
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns url for given procedure.
     * Used to communicate with PayU.
     * 
     * @param string $procedureName Name of the procedure
     * @return string
     */
    public function getUrl($procedureName) {
        return str_replace('[procedure]', $procedureName, $this->urlTemplate);
    }

    /**
     * Returns url for "new payment" procedure
     * @return string
     */
    public function getUrlForNewPayment() {
        return $this->getUrl('NewPayment');
    }

    /**
     * Returns encoding of the url used to communicate with PayU
     * @return string
     */
    public function getUrlEncoding() {
        return $this->urlEncoding;
    }

    /**
     * Sets encoding of the url used to communicate with PayU
     * 
     * @param string $urlEncoding The encoding
     * @return \Meritoo_PayU_Configuration
     */
    public function setUrlEncoding($urlEncoding) {
        $this->urlEncoding = $urlEncoding;
        return $this;
    }

    /**
     * Returns ID of payment point, the pos_id parameter
     * @return string
     */
    public function getPosId() {
        return $this->posId;
    }

    /**
     * Returns the first key
     * @return string
     */
    public function getKey1() {
        return $this->key1;
    }

    /**
     * Returns the second key
     * @return string
     */
    public function getKey2() {
        return $this->key2;
    }

    /**
     * Returns payment authentication key, the pos_auth_key parameter
     * @return string
     */
    public function getPosAuthenticationKey() {
        return $this->posAuthenticationKey;
    }

    /**
     * Returns configuration for the url used to retrieve payment types
     * @return \Zend_Config_Ini
     */
    public function getPaymentTypesUrlParameters() {
        return $this->configuration->payment_types->url;
    }

    /**
     * Returns names of parameters required by transaction
     * @return \Zend_Config_Ini
     */
    public function getTransactionRequiredParameters() {
        return $this->transactionRequiredParameters;
    }

    /**
     * Returns name of the route for url where user is redirected when transaction is finished.
     * 
     * @param [boolean $positive = true] If is set to true, name of the route for positive transaction is returned. Otherwise - for negative.
     * @return string
     */
    public function getTransactionFinishedRouteName($positive = true) {
        return $this->transactionFinishedRouteNames[$positive];
    }

    /**
     * Returns template for the description of payment.
     * The description is used to describe a payment in the PayU panel and is send as the payment's parameter.
     * 
     * @return string
     */
    public function getPaymentDescriptionTemplate() {
        return $this->configuration->payment->description->template;
    }

    /**
     * Returns information if the "sig" parameter is enabled and should be verified
     * @return boolean
     */
    public function isSigEnabled() {
        return (boolean) $this->configuration->transaction->verification->is_sig_enabled;
    }

    /**
     * Loads configuration of the PayU
     * @return \Meritoo_PayU_Configuration
     */
    private function loadConfiguration() {
        $filePath = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'application.ini';
        $this->configuration = new Zend_Config_Ini($filePath, $this->sectionName);

        if (!empty($this->configuration->url->domain) && !empty($this->configuration->url->template)) {
            $domain = $this->configuration->url->domain;
            $template = $this->configuration->url->template;
            $encoding = null;

            if (!empty($this->configuration->url->encoding)) {
                $encoding = $this->configuration->url->encoding;
            }

            $search = array('[domain]');
            $replace = array($domain);

            if (!empty($encoding)) {
                $search[] = '[encoding]';
                $replace[] = $encoding;
            }

            $this->urlTemplate = str_replace($search, $replace, $template);
            $this->urlEncoding = $encoding;
        }

        if (!empty($this->configuration->pos_id)) {
            $this->posId = $this->configuration->pos_id;
        }

        if (!empty($this->configuration->key1)) {
            $this->key1 = $this->configuration->key1;
        }

        if (!empty($this->configuration->key2)) {
            $this->key2 = $this->configuration->key2;
        }

        if (!empty($this->configuration->pos_auth_key)) {
            $this->posAuthenticationKey = $this->configuration->pos_auth_key;
        }

        if (!empty($this->configuration->transaction->required->parameters)) {
            $this->transactionRequiredParameters = $this->configuration->transaction->required->parameters;
        }

        if (!empty($this->configuration->transaction->positive->route)) {
            $this->transactionFinishedRouteNames[true] = $this->configuration->transaction->positive->route;
        }

        if (!empty($this->configuration->transaction->negative->route)) {
            $this->transactionFinishedRouteNames[false] = $this->configuration->transaction->negative->route;
        }

        return $this;
    }

}