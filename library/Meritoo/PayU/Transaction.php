<?php

/**
 * The transaction
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_PayU_Transaction {

    /**
     * Parameters of the transaction
     * @var array
     */
    private $parameters = array();

    /**
     * Returns parameters of the transaction
     * 
     * @param [boolean $asNamesValues = false] If is set to true, returns parameters as simple array with pairs name-value. Otherwise - as an array with objects.
     * @return array
     */
    public function getParameters($asNamesValues = false) {
        $parameters = $this->parameters;

        if (!empty($parameters) && $asNamesValues) {
            $effect = array();

            foreach ($parameters as $parameter) {
                /* @var $parameter Meritoo_PayU_Transaction_Parameter */
                $name = $parameter->getName();
                $value = $parameter->getValue();

                $effect[$name] = $value;
            }

            $parameters = $effect;
        }

        return $parameters;
    }

    /**
     * Sets parameters of the transaction
     * 
     * @param array $parameters The parameters
     * @return \Meritoo_PayU_Transaction
     */
    public function setParameters(array $parameters) {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Adds a new parameter
     * 
     * @param \Meritoo_PayU_Transaction_Parameter $parameter The new parameter
     * @return \Meritoo_PayU_Transaction
     */
    public function addParameter(\Meritoo_PayU_Transaction_Parameter $parameter) {
        $name = $parameter->getName();
        $this->parameters[$name] = $parameter;

        return $this;
    }

    /**
     * Sets value of given parameter
     * 
     * @param string $name Name of the parameter
     * @param mixed $value Value of the parameter
     * @return \Meritoo_PayU_Transaction
     * 
     * @throws \Exception
     */
    public function setParameterValue($name, $value) {
        if (!isset($this->parameters[$name])) {
            throw new Zend_Exception(sprintf('There is no parameter with name "%s"', $name));
        }

        /* @var $parameter \Meritoo_PayU_Transaction_Parameter */
        $parameter = $this->parameters[$name];
        $parameter->setValue($value);

        return $this;
    }

    /**
     * Sets values of given parameters
     * 
     * @param array $values The key-value pairs where: key - it's a name of parameter, value - value of parameter
     * @return \Meritoo_PayU_Transaction
     */
    public function setParametersValues(array $values) {
        if (!empty($values)) {
            foreach ($values as $name => $value) {
                $this->setParameterValue($name, $value);
            }
        }

        return $this;
    }

    /**
     * Sends and starts the transaction
     * @return void
     */
    public function send() {
        $withoutValues = $this->haveRequiredParametersValues();

        if (is_array($withoutValues)) {
            throw new Zend_Exception(sprintf('Transaction cannot be send, because these required parameters haven\'t values: %s.', implode(', ', $withoutValues)));
        }

        $configuration = Meritoo_PayU_Configuration::getInstance();
        $handle = curl_init();
        $returnTransfer = false;
        //$returnTransfer = true;

        if ($configuration->isSigEnabled()) {
            $sigValue = $this->getSigValue();

            if (empty($sigValue)) {
                throw new Zend_Exception('The "sig" parameter verification is enabled, but generated value of the "sig" parameter is empty. Something is broken...');
            }

            $this->addParameter(new Meritoo_PayU_Transaction_Parameter('sig', $sigValue));
        }

        curl_setopt_array($handle, array(
            CURLOPT_URL => $configuration->getUrlForNewPayment(),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $this->getParameters(true),
            CURLOPT_RETURNTRANSFER => $returnTransfer
        ));

        $result = curl_exec($handle);
        //var_dump($result);die;

        if ($returnTransfer && $result === false) {
            $errorNumber = curl_errno($handle);
            $error = curl_error($handle);

            throw new Zend_Exception(sprintf('An error occurred: %s. Error number: %s', $error, $errorNumber));
        }

        //var_dump($result);die;
    }

    /**
     * Returns value for the "sig" parameter.
     * This parameter should be send with all other parameters for the new payment.
     * 
     * @return string
     */
    private function getSigValue() {
        $sigString = '';
        $sigValue = null;

        /*
         * All parameters used to generate value of the "sig" parameter
         */
        $sigParameters = array(
            'pos_id',
            'pay_type',
            'session_id',
            'pos_auth_key',
            'amount',
            'desc',
            'desc2',
            'trsDesc',
            'order_id',
            'first_name',
            'last_name',
            'street',
            'street_hn',
            'street_an',
            'city',
            'post_code',
            'country',
            'email',
            'phone',
            'language',
            'client_ip',
            'ts',
                //'key1' // The "key1" parameter is skipped, because is not send as the transaction's parameter
        );

        /*
         * Formula for the "sig" value:
         * sig = md5 (pos_id + pay_type + session_id + pos_auth_key + amount + desc + desc2 + trsDesc + order_id + first_name + last_name + street + street_hn + street_an + city + post_code + country + email + phone + language + client_ip + ts + key1)
         */

        /*
         * Building the proper string used to generate value
         */
        $parameters = $this->getParameters(true);

        foreach ($sigParameters as $parameter) {
            if (isset($parameters[$parameter])) {
                $sigString .= $parameters[$parameter];
            }
        }

        /*
         * Inserting the last parameter, the "key1".
         * It's the fist key from the payment point's configuration.
         */
        if (!empty($sigString)) {
            $sigString .= Meritoo_PayU_Configuration::getInstance()
                    ->getKey1();

            $sigValue = md5($sigString);
        }

        return $sigValue;
    }

    /**
     * Returns information if required have parameters values
     * @return boolean
     */
    private function haveRequiredParametersValues() {
        $withoutValues = array();

        if (!empty($this->parameters)) {
            foreach ($this->parameters as $parameter) {
                /* @var $parameter \Meritoo_PayU_Transaction_Parameter */
                if ($parameter->isRequired() && $parameter->getValue() === null) {
                    $withoutValues[] = $parameter->getName();
                }
            }
        }

        if (empty($withoutValues)) {
            return true;
        }

        return $withoutValues;
    }

}