<?php

/**
 * Helper for the transaction
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_PayU_Transaction_Helper {

    /**
     * Class instance (serve a singleton pattern purpose)
     * @var \Meritoo_PayU_Transaction_Helper
     */
    private static $instance;

    /**
     * The main key used by session to store transactions' data
     * @var string
     */
    private $sessionKeyMain = 'transaction';

    /**
     * The key used by session to store latest transaction's data
     * @var string
     */
    private $sessionKeyLatestTransaction = 'latestTransaction';

    /**
     * Class constructor
     * @return void
     */
    private function __construct() {
        
    }

    /**
     * Returns class instance (serve a singleton pattern purpose)
     * @return \Meritoo_PayU_Transaction_Helper
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Creates as new / empty transaction
     * @return \Meritoo_PayU_Transaction
     */
    public function createTransaction() {
        $transaction = new Meritoo_PayU_Transaction();

        $requiredParametersNames = Meritoo_PayU_Configuration::getInstance()
                ->getTransactionRequiredParameters();

        if (!empty($requiredParametersNames)) {
            $value = null;
            $required = true;

            foreach ($requiredParametersNames as $name) {
                $parameter = new Meritoo_PayU_Transaction_Parameter($name, $value, $required);
                $transaction->addParameter($parameter);
            }
        }

        return $transaction;
    }

    /**
     * Returns session used by the transaction's helper
     * @return \Zend_Session_Namespace
     */
    public function getSession() {
        return new Zend_Session_Namespace($this->sessionKeyMain);
    }

    /**
     * Stores / saves the latest transaction
     * 
     * @param mixed $transactionData The transaction's data
     * @return \Meritoo_PayU_Transaction_Helper
     */
    public function storeLatestTransaction($transactionData) {
        $session = $this->getSession();
        $key = $this->sessionKeyLatestTransaction;

        $session->{$key} = $transactionData;
        return $this;
    }

    /**
     * Returns the latest transaction
     * @return mixed
     */
    public function getLatestTransaction() {
        return $this->getSession()
                ->{$this->sessionKeyLatestTransaction};
    }

    /**
     * Clears the latest transaction
     * @return \Meritoo_PayU_Transaction_Helper
     */
    public function clearLatestTransaction() {
        $session = $this->getSession();
        unset($session->{$this->sessionKeyLatestTransaction});
    }

}