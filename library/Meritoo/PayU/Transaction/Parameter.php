<?php

/**
 * Parameter of the transaction
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_PayU_Transaction_Parameter {

    /**
     * Name of the parameter
     * @var string
     */
    private $name = '';

    /**
     * Value of the parameter
     * @var mixed
     */
    private $value = null;

    /**
     * Information if the parameter is required
     * @var boolean
     */
    private $required = false;

    /**
     * Class constructor
     * 
     * @param string $name Name of the parameter
     * @param [mixed $value = null] Value of the parameter
     * @param [boolean $required = false] Information if the parameter is required
     * @return void
     */
    public function __construct($name, $value = null, $required = false) {
        $this->setName($name)
                ->setValue($value)
                ->setAsRequired($required);
    }

    /**
     * Returns name of the parameter
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets name of the parameter
     * 
     * @param string $name The name
     * @return \Meritoo_PayU_Transaction_Parameter
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns value of the parameter
     * @return mixed
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Sets value of the parameter
     * 
     * @param mixed $value The value
     * @return \Meritoo_PayU_Transaction_Parameter
     */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * Returns information if the parameter is required
     * @return boolean
     */
    public function isRequired() {
        return $this->required;
    }

    /**
     * Sets information if the parameter is required
     * 
     * @param [boolean $required = true] Information if the parameter is required
     * @return \Meritoo_PayU_Transaction_Parameter
     */
    public function setAsRequired($required = true) {
        $this->required = $required;
        return $this;
    }

}